package fr.ouissemouerdiane.EcommerceApp.dao;


import fr.ouissemouerdiane.EcommerceApp.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:4200")
@RepositoryRestResource(collectionResourceRel = "productCategory", path = "product-category")// cette ligne permet de renommer le resultat du fichier Json et de ne pas mettre s à la fin
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {
}
